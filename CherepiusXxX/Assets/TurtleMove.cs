using System.Security.AccessControl;
using System.ComponentModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TurtleMove : MonoBehaviour
{
    // Start is called before the first frame update
        public Image im;
        private Vector3 mousePos;
        private Vector3 mousePos2;
        private float speed = 0.1f;
        private bool f = false;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
      if(Input.GetMouseButtonUp(0))
      {
          mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
          f = true;
          Debug.Log(mousePos.ToString());
          im.transform.Rotate(new Vector3(0.0f,0.0f, mousePos.z));
      }  
    }
    private void FixedUpdate()
    {
         if(f)
        {
            if(mousePos != im.transform.position)
            {
                MoveObj();
            }
            else
            {
                f = false;
            }
        }
     }
    public void MoveObj()
    {
        mousePos2 = new Vector3 (mousePos.x, mousePos.y, 0 );
        im.transform.position = Vector3.MoveTowards(im.transform.position, mousePos2, speed);
    }
}
